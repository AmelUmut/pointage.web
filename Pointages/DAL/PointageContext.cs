﻿using Pointages.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace Pointages.DAL

{
    public class PointageContext:DbContext
    {
        public PointageContext() : base("PointageContext")
        {
        }

        public DbSet<Pointeuse> Pointeuses { get; set; }
       // public DbSet<Pointeuse> Pointeuses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}