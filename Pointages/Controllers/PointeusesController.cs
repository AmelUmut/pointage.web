﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pointages.DAL;
using Pointages.Shared;
using Pointages.Models;
namespace Pointages.Controllers
{
    public class PointeusesController : Controller
    {
        private PointageContext db = new PointageContext();
        // GET: Pointeuses
        public ActionResult Index()
        {
            return View(db.Pointeuses.ToList());
        }

        // GET: Pointeuses/Edit/5
        public ActionResult Connect(int? id)
        {
            GlobalVariables.axCZKEM.Disconnect();
            Pointeuse pointeuse = db.Pointeuses.Find(id);
            //try connect
            if (GlobalVariables.axCZKEM.Connect_Net(pointeuse .IP,pointeuse .Port) == true)
                TempData["Success"] = "Pointeuse Connectée";

            else
                TempData["Success"] = "Pointeuse non Connectée";

            return RedirectToAction("Index");
        }

        public ActionResult DisConnect(int? id)
        {

            Pointeuse pointeuse = db.Pointeuses.Find(id);
            //try connect
            GlobalVariables.axCZKEM.Disconnect();

            TempData["Success"] = "Pointeuse Déconnectée";

            return RedirectToAction("Index");
        }
        public ActionResult Read(int? id)
        {

            Pointeuse pointeuse = db.Pointeuses.Find(id);
            
            String data=Pointeuse.GetGeneralLogData(GlobalVariables.axCZKEM, pointeuse.IP, pointeuse.Port);
            
            TempData["Success"] = data;

            return RedirectToAction("Index");
        }
        // GET: Pointeuses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pointeuse pointeuse = db.Pointeuses.Find(id);
            if (pointeuse == null)
            {
                return HttpNotFound();
            }
            return View(pointeuse);
        }

        // GET: Pointeuses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pointeuses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,IP,Port")] Pointeuse pointeuse)
        {
            if (ModelState.IsValid)
            {
                db.Pointeuses.Add(pointeuse);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pointeuse);
        }

        // GET: Pointeuses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pointeuse pointeuse = db.Pointeuses.Find(id);
            if (pointeuse == null)
            {
                return HttpNotFound();
            }
            return View(pointeuse);
        }


        // POST: Pointeuses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,IP,Port")] Pointeuse pointeuse)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pointeuse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pointeuse);
        }

        // GET: Pointeuses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pointeuse pointeuse = db.Pointeuses.Find(id);
            if (pointeuse == null)
            {
                return HttpNotFound();
            }
            return View(pointeuse);
        }

        // POST: Pointeuses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pointeuse pointeuse = db.Pointeuses.Find(id);
            db.Pointeuses.Remove(pointeuse);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
