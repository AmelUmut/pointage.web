﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pointages.Models
{
    [Table("Pointeuse", Schema = "public")]
    public class Pointeuse 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
        bool Isprinc { get; set; }

        
        public static string GetGeneralLogData(zkemkeeper.CZKEM axCZKEM1, string ip, int port)
        {
            String data = "";
            if (axCZKEM1.Connect_Net(ip, port) == false)
            {
                data = "Please connect the device first";

            }
            else
            {
                string sdwEnrollNumber = "";
                //int idwTMachineNumber=0;
                //int idwEMachineNumber=0;
                int idwVerifyMode = 0;
                int idwInOutMode = 0;
                int idwYear = 0;
                int idwMonth = 0;
                int idwDay = 0;
                int idwHour = 0;
                int idwMinute = 0;
                int idwSecond = 0;
                int idwWorkcode = 0;

                int idwErrorCode = 0;
                int iGLCount = 0;
                int iIndex = 0;
                
                axCZKEM1.EnableDevice(1, false);//disable the device
                if (axCZKEM1.ReadGeneralLogData(1))//read all the attendance records to the memory
                {
                    while (axCZKEM1.SSR_GetGeneralLogData(1, out sdwEnrollNumber, out idwVerifyMode,
                               out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                    {
                        iGLCount++;
                 
                        data = data + sdwEnrollNumber+" "+ idwVerifyMode.ToString()+" "+ idwInOutMode.ToString()+" "+ idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString();
                        iIndex++;
                    }
                }
                else
                {
                   
                    axCZKEM1.GetLastError(ref idwErrorCode);

                    if (idwErrorCode != 0)
                    {
                        data="Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString();
                    }
                    else
                    {
                        data="No data from terminal returns!";
                    }
                }
                axCZKEM1.EnableDevice(1, true);//enable the device
            }
                return data;
        }

    }

}